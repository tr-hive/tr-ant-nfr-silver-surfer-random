/// <reference path="../typings/tsd.d.ts" />
var rabbit = require("da-rabbitmq-rx");
var logs = require("da-logs");
var handler = require("./handler");
var trAnt = require("tr-ant-utils");
var getEnvVar = trAnt.getEnvVar;
var RABBIT_URI = getEnvVar("RABBIT_URI");
var RABBIT_QUEUE_NOTIFS = getEnvVar("RABBIT_QUEUE_NOTIFS");
var LOG_LOGGLY_KEY = getEnvVar("LOG_LOGGLY_KEY");
var LOG_LOGGLY_SUBDOMAIN = getEnvVar("LOG_LOGGLY_SUBDOMAIN");
var LOG_MONGO_URI = getEnvVar("LOG_MONGO_URI");
var LOG_MONGO_COLLECTION = getEnvVar("LOG_MONGO_COLLECTION");
var TICKETS = getEnvVar("SILVER_SURFER_RANDOM_TICKETS").split(",");
var MAX_TIMEOUT = parseInt(getEnvVar("SILVER_SURFER_MAX_TIMEOUT"));
var MIN_TIMEOUT = parseInt(getEnvVar("SILVER_SURFER_MIN_TIMEOUT"));
var logger = new logs.LoggerCompose({ pack: require("../package.json"), tags: [] }, {
    loggly: { token: LOG_LOGGLY_KEY, subdomain: LOG_LOGGLY_SUBDOMAIN },
    mongo: { connection: LOG_MONGO_URI, collection: LOG_MONGO_COLLECTION },
    console: true
});
var pubOpts = { uri: RABBIT_URI, socketType: rabbit.SocketType.PUB, queue: RABBIT_QUEUE_NOTIFS };
var pub = new rabbit.RabbitPub(pubOpts);
pub.connect();
pub.connectStream.subscribe(function () {
    return logger.write({ resource: "rabbit", oper: "connected", status: "success", opts: pubOpts });
}, function (err) {
    logger.write({ resource: "rabbit", status: "error", err: err, opts: pubOpts });
    process.exit(1);
});
var handlerOpts = {
    logger: logger,
    pub: pub,
    tickets: TICKETS,
    minTimeout: MIN_TIMEOUT,
    maxTimeout: MAX_TIMEOUT
};
handler.handle(handlerOpts);
