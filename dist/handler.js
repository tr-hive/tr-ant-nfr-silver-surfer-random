/// <reference path="../typings/tsd.d.ts" />
var Rx = require("rx");
var uuid = require("node-uuid");
var PKG_NAME = require("../package.json").name;
var ORDERS = ["buy", "sell"];
var newDate = function () { return new Date(); };
var newUUID = function () { return uuid.v4(); };
function getRandom(min, max) {
    return Math.random() * (max - min) + min;
}
function handle(opts) {
    Rx.Observable
        .generateWithRelativeTime(null, function (_) { return true; }, function (_) { return _; }, function (_) { return _; }, function (_) { return getRandom(opts.minTimeout, opts.maxTimeout); })
        .startWith(null)
        .subscribe(function (_) { return handleOne(opts); });
}
exports.handle = handle;
function handleOne(opts) {
    if (opts.newDate)
        newDate = opts.newDate;
    if (opts.newUUID)
        newUUID = opts.newUUID;
    var ticker = opts.tickets[Math.round(getRandom(0, opts.tickets.length - 1))];
    var stop = getRandom(1, 100);
    var force = getRandom(1, 10);
    var oper = ORDERS[Math.round(getRandom(0, 1))];
    var tmt = getRandom(opts.minTimeout, opts.maxTimeout);
    var notif = {
        key: newUUID(),
        issuer: PKG_NAME,
        type: "INotifSilverSurferData",
        date: (newDate()).toISOString(),
        data: {
            ticket: ticker,
            oper: oper,
            stop: stop,
            force: force
        }
    };
    opts.pub.write(notif);
    opts.logger.write({ oper: "handle", status: "success", notif: notif });
}
