/// <reference path="../typings/tsd.d.ts" />

import Rx = require("rx");
import logs = require("da-logs");
import rabbit = require("da-rabbitmq-rx");
var uuid = require("node-uuid");
import trAnt =require("tr-ant-utils");

const PKG_NAME = require("../package.json").name;

const ORDERS = ["buy", "sell"];

var newDate = () => new Date();  
var newUUID = () => uuid.v4();

export interface IHandlerOpts {
	logger: logs.ILogger 	
	pub: rabbit.RabbitPub
  tickets: string[]
	minTimeout: number  
	maxTimeout: number
	newDate?() : Date
	newUUID?() : string  
}

function getRandom(min: number, max: number): number {
  return Math.random() * (max - min) + min;
}

export function handle(opts : IHandlerOpts) {
 
  Rx.Observable
  .generateWithRelativeTime(null, _ => true, _ => _, _ => _, _ => getRandom(opts.minTimeout, opts.maxTimeout))
  .startWith(null)
  .subscribe(_ => handleOne(opts));
}

function handleOne(opts : IHandlerOpts) {
	if (opts.newDate)
		newDate = opts.newDate;
	if (opts.newUUID)		
		newUUID = opts.newUUID;
		
  var ticker = opts.tickets[Math.round(getRandom(0, opts.tickets.length - 1))];
  var stop = getRandom(1, 100);
  var force = getRandom(1, 10);
  var oper = ORDERS[Math.round(getRandom(0, 1))];
  var tmt = getRandom(opts.minTimeout, opts.maxTimeout);

  var notif : trAnt.INotif<trAnt.INotifSilverSurferData> = {
    key : newUUID(),
    issuer : PKG_NAME,
    type: "INotifSilverSurferData",
    date: (newDate()).toISOString(),
    data : {
      ticket: ticker,
      oper: oper,
      stop: stop,
      force: force
    }
  };
  opts.pub.write(notif);
  opts.logger.write({oper: "handle", status: "success", notif : notif});			
}
